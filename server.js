const express = require('express')
const app = express()
const port = process.env.PORT || 3000

app.get('/', (req, res) => {
    let result = {
        message: "Hello World!"
    }
    res.status(200).json(result)
})

app.listen(port, () => {
  console.log(`Example app listening at port ${port}`)
})